use nannou::prelude::*;

pub struct Node {
    pub i: u32,
    pub u: f32,
    pub v: f32,
    pub x: f32,
    pub y: f32,
    pub r: f32,
    pub a: f32,
    pub m: f32,
    pub dev: f32,
}

impl Node {
    pub fn new(i: u32, _u: f32, _v: f32, r: f32) -> Node {
        let a = random_range(-1.0, 1.0);
        let m = random_f32();
        let u = (a * PI * 2.0).sin() * random_range(0.0, 0.2);
        let v = (a * PI * 2.0).cos() * random_range(0.0, 0.2);

        Node {
            i,
            u,
            v,
            r,
            x: 0.0,
            y: 0.0,
            a,
            m,
            dev: random_range(-1.0, 1.0),
        }
    }

    pub fn xy(&self) -> Point2 {
        pt2(self.x, self.y)
    }
}
