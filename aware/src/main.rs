use nannou::prelude::*;
mod node;
use node::Node;

fn main() {
    nannou::app(model).update(update).run();
}

struct Model {
    playing: bool,
    hue: f32,
    clear_frame: u64,
    node_count: usize,
    node_radius: f32,
    nodes: Vec<Node>,
    connections: Vec<(usize, usize)>,
    t: f32,
}

fn key_pressed(app: &App, model: &mut Model, key: Key) {
    if key == Key::Space {
        model.nodes = create_nodes(model.node_count, model.node_radius);
        model.clear_frame = app.elapsed_frames() + 2;
    } else if key == Key::Up {
        model.node_count *= 2;
        model.nodes = create_nodes(model.node_count, model.node_radius);
        model.clear_frame = app.elapsed_frames() + 2;
    } else if key == Key::Down {
        model.node_count /= 2;
        model.nodes = create_nodes(model.node_count, model.node_radius);
        model.clear_frame = app.elapsed_frames() + 2;
    } else if key == Key::Right {
        model.hue += 0.025;
    } else if key == Key::Left {
        model.hue -= 0.025;
    } else if key == Key::Return {
        model.playing = !model.playing;
    } else if key == Key::S {
        app.main_window()
            .capture_frame(app.exe_name().unwrap() + ".png");
    }

    model.node_radius = model.node_count as f32 / model.node_count as f32 * 3.0;
}

fn model(app: &App) -> Model {
    app.new_window()
        .decorations(false)
        .size(1200, 1200)
        .key_pressed(key_pressed)
        .view(view)
        .build()
        .unwrap();

    let node_count = 100;
    let node_radius = node_count as f32 * 0.05;
    let mut nodes = create_nodes(node_count, node_radius);
    let connections = get_close_connections(&mut nodes);

    Model {
        playing: true,
        clear_frame: 1,
        hue: 0.0,
        node_count,
        node_radius,
        nodes,
        connections,
        t: 0.0,
    }
}

fn create_nodes(node_count: usize, node_radius: f32) -> Vec<Node> {
    (0..node_count + 1)
        .map(|i| {
            let u = 0.0; //random_range(-1.0, 1.0);
            let v = 0.0; //random_range(-1.0, 1.0);
                         // let u = (i as f32 / node_count as f32) * 2.0 - 1.0;
            return Node::new(i as u32, u, v, node_radius);
        })
        .collect()
}

fn get_close_connections(nodes: &mut Vec<Node>) -> Vec<(usize, usize)> {
    let mut connections = vec![];
    let mut mapped = vec![];

    (0..nodes.len()).for_each(|i| {
        let mut closest_dist = std::f32::MAX;
        let mut closest_index = 0;

        // if i + 1 is in the array don't

        (0..nodes.len()).for_each(|j| {
            if i == j {
                return;
            }

            // if mapped.contains(&j) {
            //     return;
            // }
            // stuff
            let a = nodes[i].xy();
            let b = nodes[j].xy();

            let d = a.distance(b);

            let dist = 10.0;
            if d < dist {
                // get angle of shit
                let mag = (dist - d).abs() / dist;

                // nodes[i].a += nodes[i].dev * mag * 0.0002;
                // nodes[j].a += nodes[j].dev * mag * 0.0002;

                let angle = nodes[i].xy().angle_between(nodes[j].xy());
                nodes[i].a += ((nodes[i].a * PI * 2.0) - angle) * mag * 0.0001;
                nodes[i].m += mag * 0.001;
                nodes[i].dev += nodes[j].dev * 0.00001 * mag;
            // repel with force
            } else {
                if nodes[i].m > 0.5 {
                    nodes[i].m *= 0.99
                }
            }

            if d < closest_dist {
                closest_dist = d;
                closest_index = j;
            }
        });

        // mapped.push(i);
        mapped.push(closest_index);

        let map = (i, closest_index);

        connections.push(map);
    });
    return connections;
}

fn update(app: &App, model: &mut Model, _update: Update) {
    if !model.playing {
        return;
    }
    let (w, h) = app.main_window().inner_size_pixels();
    let w = w as f32;
    let h = h as f32;
    let hratio = w / h;
    let wratio = h / w;

    model.t += 0.05;

    for node in model.nodes.iter_mut() {
        node.u += (node.a * PI * 2.0).sin() * 0.006 * node.m;
        node.v += (node.a * PI * 2.0).cos() * 0.006 * node.m;

        if node.u > 1.0 {
            node.u = -1.0
        }
        if node.u < -1.0 {
            node.u = 1.0
        }

        if node.v > 1.0 {
            node.v = -1.0
        }
        if node.v < -1.0 {
            node.v = 1.0
        }

        node.m *= 0.995;

        node.x = node.u * 0.9 * w * 0.5 * hratio;
        node.y = node.v * 0.9 * h * 0.5 * wratio;
    }

    model.connections = get_close_connections(&mut model.nodes);
}

fn view(app: &App, model: &Model, frame: Frame) {
    if !model.playing {
        return;
    }
    let draw = app.draw();
    let (w, h) = app.main_window().inner_size_pixels();
    // let mouse_position = app.mouse.position();
    // println!("{:?}", mouse_position / vec2(w, h) * 2.0);

    if app.elapsed_frames() < model.clear_frame {
        draw.background().hsl(model.hue, 1.0, 0.15);
    }

    draw.rect()
        .x_y(0.0, 0.0)
        .w_h(w as f32, h as f32)
        .hsla(model.hue, 1.0, 0.15, 0.025);

    for (i, j) in model.connections.iter() {
        draw.line()
            .start(model.nodes[*i].xy())
            .end(model.nodes[*j].xy())
            .stroke_weight(model.node_radius / 3.0)
            .hsl(1.0, 1.0, 1.0);
    }

    for node in &model.nodes {
        draw.ellipse().xy(node.xy()).radius(node.r);
    }

    draw.to_frame(app, &frame).unwrap();
}
