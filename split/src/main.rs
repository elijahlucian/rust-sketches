use nannou::geom::Point2;
use nannou::noise::*;
use nannou::prelude::*;
use wallpaper;

const AGENT_COUNT: usize = 500;
const WIDTH: u32 = 1200;
const HEIGHT: u32 = 1200;
const MAX_AGE: u32 = 50;

fn main() {
    nannou::app(model).update(update).run()
}

struct Model {
    agents: Vec<Agent>,
    noise: SuperSimplex,
    time: f32,
    scale_x: f32,
    scale_y: f32,
    speed: f32,
    stop: bool,
    fill_until: u64,
}

struct Agent {
    pub u: f32,
    pub v: f32,
    pub u2: f32,
    pub v2: f32,
    pub xdir: u8,
    pub ydir: u8,
    pub size: f32,
    pub i: u32,
    pub angle: f32,
    pub hue: f32,
    pub n: f32,
    pub step_scale: f32,
    pub day: f32,
    pub f: u32,
    pub z: f32,
    pub next: bool,
    pub adult: bool,
    pub drawn: bool,
    pub parent: Option<usize>,
    pub children: Vec<Box<Agent>>,
    pub generation: u32,
}

impl Agent {
    fn new(
        u: f32,
        v: f32,
        size: f32,
        i: u32,
        n: f32,
        step_scale: f32,
        hue: f32,
        parent: Option<Box<Agent>>,
        generation: u32,
    ) -> Agent {
        Agent {
            u,
            v,
            u2: u,
            v2: v,
            xdir: 0,
            ydir: 0,
            size,
            i,
            hue,
            n,
            angle: n,
            step_scale,
            f: 0,
            drawn: false,
            day: 1.0,
            next: true,
            adult: false,
            z: n * PI * 2.0,
            children: vec![],
            parent: None,
            generation,
        }
    }

    fn update(&mut self, dt: f32, n: f32) {
        for child in self.children.iter_mut() {
            child.update(dt, n);
        }

        if self.f > 1 {
            return;
        }

        self.f += 1;
        if self.adult {
            return;
        }

        // self.hue = ((self.angle + n * 0.1) * PI * 2.0).sin();

        self.u2 = self.u + (sin(self.angle, self.step_scale)) * self.day;
        self.v2 = self.v + (cos(self.angle, self.step_scale)) * self.day;

        if self.day < 1.0 {
            self.day += dt;
        } else {
            self.adult = true;

            if self.step_scale > 3.0 || self.generation > MAX_AGE {
                return;
            }

            // if random_f32() > 0.7 {//0.662 {
            //     return;
            // }

            let rng = 1; //random_range(1, 2);

            let mut agents = vec![];

            let gu = (self.generation as f32 / MAX_AGE as f32);

            let agent = Agent::new(
                self.u2,
                self.v2,
                self.size * random_range(0.95, 1.25),
                self.i + random_range(1, 400),
                n * 0.2 + (gu * random_range(-0.05, 0.05)), // * gu * (self.u2.abs() + self.v2.abs()),
                self.step_scale * random_range(0.95, 1.05), //+ n * 0.01,
                self.hue + n * 0.015 * gu,
                None,
                self.generation + 1,
            );

            agents.push(Box::new(agent));
            self.children = agents;
        }
    }

    fn draw(&self, draw: &Draw, w: f32, h: f32) {
        for child in self.children.iter() {
            child.draw(&draw, w, h);
        }

        if self.f > 1 {
            return;
        }

        // let ou = ((self.u2 * PI * 2.0 * 10.0).sin() * (self.v2 * PI * 2.0 * 10.0).cos());
        let gu = self.generation as f32 / MAX_AGE as f32;

        let gg = (1.0 - gu).abs();

        &draw
            .line()
            .weight(self.size * gg)
            .points(
                Point2 {
                    x: self.u * w,
                    y: self.v * h,
                },
                Point2 {
                    x: self.u2 * w,
                    y: self.v2 * h,
                },
            )
            .hsla(self.hue + (random_f32() - 0.5) * 0.2, 1.0, 0.5, 0.5);
    }
}

fn cos(n: f32, scale: f32) -> f32 {
    (n * PI * 2.0).cos() * scale
}

fn sin(n: f32, scale: f32) -> f32 {
    (n * PI * 2.0).sin() * scale
}

fn reset_agents(count: usize) -> Vec<Agent> {
    let mut agents = vec![];

    let h = random_f32();

    for i in 1..count {
        // let u = (i as f64 / count as f64 * PI_F64 * 2.0) as f32;

        let u = random_f32() * 2.0 - 1.0;
        let v = random_f32() * 2.0 - 1.0;
        let angle = random_f32(); // * 0.025;

        let hue = if random_f32() > 0.5 {
            h
        } else {
            (0.5 - h).abs()
        };

        agents.push(Agent::new(
            u, //-0.0,
            v, // -0.0,
            random_range(2.0, 6.0),
            i as u32,
            angle,
            0.007,
            hue,
            None,
            0,
        ));
    }
    return agents;
}

fn model(app: &App) -> Model {
    app.new_window()
        .size(WIDTH, HEIGHT)
        .event(event)
        .view(view)
        .build()
        .unwrap();

    let mut noise = SuperSimplex::new();
    noise = noise.set_seed(222);

    Model {
        agents: reset_agents(AGENT_COUNT),
        time: 0.0,
        noise,
        scale_x: 1.0,
        scale_y: 1.0,
        speed: 1.0,
        stop: false,
        fill_until: 2,
    }
}

fn event(app: &App, model: &mut Model, window_event: WindowEvent) {
    match window_event {
        // MouseMoved(pos) => {
        //     model.scale_x = pos.x * 0.1;
        //     model.scale_y = pos.y * 0.1;
        // }
        KeyPressed(key) => match key {
            Key::Up => {
                model.speed += 0.1;
            }
            Key::Down => {
                model.speed -= 0.1;
            }
            Key::Left => {
                model.scale_x -= 1.0;
                model.scale_y -= 1.0;
            }
            Key::Right => {
                model.scale_x += 1.0;
                model.scale_y += 1.0;
            }
            Key::Space => {
                model.agents = reset_agents(AGENT_COUNT);
                model.fill_until = app.elapsed_frames() + 2;
                // model.stop = true;
                // let mut noise = SuperSimplex::new();
                // noise = noise.set_seed(app.elapsed_frames() as u32);
                // model.noise = noise;
            }
            Key::S => {
                app.main_window()
                    .capture_frame(app.exe_name().unwrap() + ".png");

                // wallpaper::set_from_path(
                //     &app.project_path().unwrap().to_str().unwrap().to_owned() + "/split.png",
                // )
            }
            _ => {}
        },
        _ => {}
    }

    // println!(
    //     "Speed: {}, ScaleX: {}, ScaleY: {}",
    //     model.speed, model.scale_x, model.scale_y
    // );
}

fn update(_app: &App, model: &mut Model, update: Update) {
    if model.stop {
        return;
    }

    let dt = update.since_last.as_secs_f32() * model.speed;
    model.time += dt;

    for agent in model.agents.iter_mut() {
        let n = model.noise.get([
            (model.scale_x * agent.u) as f64,
            (model.scale_y * agent.v) as f64,
            model.time as f64,
        ]) as f32;

        agent.update(dt, n)
    }
}

fn view(app: &App, model: &Model, frame: Frame) {
    if app.elapsed_frames() < model.fill_until {
        frame.clear(BLACK);
    }
    let draw = app.draw();
    // draw.background().hsl(0.0, 0.0, 0.0);

    let (w, h) = app.main_window().inner_size_pixels();

    let w = w as f32 * 0.5;
    let h = h as f32 * 0.5;

    let ratio = w as f32 / h as f32;

    let time_scale = 15.0;
    let t = model.time * time_scale;

    for agent in model.agents.iter() {
        agent.draw(&draw, w, h * ratio);
    }
    draw.to_frame(app, &frame).unwrap();
}
