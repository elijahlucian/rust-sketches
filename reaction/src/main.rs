use nannou::noise::*;
use nannou::prelude::*;

struct Agent {
    x: f32,
    y: f32,
}

fn main() {
    nannou::app(model).update(update).run()
}

struct Model {
    agents: Vec<Agent>,
    noise: SuperSimplex,
    time: f32,
    scale_x: f32,
    scale_y: f32,
    speed: f32,
}

fn model(app: &App) -> Model {
    app.new_window().event(event).view(view).build().unwrap();

    let mut noise = SuperSimplex::new();

    let agents = vec![];

    Model {
        agents,
        time: 0.0,
        noise,
        scale_x: 12.0,
        scale_y: 12.0,
        speed: 1.0,
    }
}

fn event(_app: &App, model: &mut Model, window_event: WindowEvent) {
    // event
}

fn update(app: &App, model: &mut Model, _update: Update) {
    let dt = model.speed * 0.005; // a;pp.fps() / 60.0 * 0.005;
    model.time += dt;
}

fn view(app: &App, model: &Model, frame: Frame) {
    // not vue
}
