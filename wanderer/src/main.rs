use nannou::noise::*;
use nannou::prelude::*;

const AGENT_COUNT: usize = 1000;
// const LAYER_COUNT: usize = 500;

fn main() {
    nannou::app(model).update(update).run()
}

struct Model {
    agents: Vec<Agent>,
    noise: SuperSimplex,
    time: f64,
    scale_x: f64,
    scale_y: f64,
    speed: f64,
}

struct Agent {
    pub u: f64,
    pub v: f64,
    pub size: f64,
    pub i: f64,
    pub n: f64,
}

impl Agent {
    fn new(u: f64, v: f64, size: f64, i: u32) -> Agent {
        Agent {
            u,
            v,
            size,
            i: i as f64,
            n: 0.0,
        }
    }

    fn update(&mut self, n: f64) {
        self.n = n;
        self.u += (n * PI as f64 * 2.0).sin() * 0.015;
        self.v += (n * PI as f64 * 2.0).cos() * 0.005;
    }
}

fn model(app: &App) -> Model {
    app.new_window().event(event).view(view).build().unwrap();
    let mut agents = vec![];
    let noise = SuperSimplex::new().set_seed(42);

    for i in 1..AGENT_COUNT {
        let u = i as f64 / AGENT_COUNT as f64;
        agents.push(Agent::new(n(u), -0.333, 1.0, i as u32));
    }

    Model {
        agents,
        time: 0.0,
        noise,
        scale_x: 2.0,
        scale_y: 2.0,
        speed: 0.2,
    }
}

fn n(x: f64) -> f64 {
    return (x - 0.5) * 2.0;
}

// fn normal_noise(x: f64) -> f64 {
//     return (x + 1.0) / 2.0;
// }

fn event(_app: &App, model: &mut Model, window_event: WindowEvent) {
    // println!("{:?}", window_event);

    match window_event {
        // MouseMoved(pos) => {
        //     model.scale_x = pos.x * 0.1;
        //     model.scale_y = pos.y * 0.1;
        // }
        KeyPressed(key) => match key {
            Key::Up => {
                model.speed += 0.1;
            }
            Key::Down => {
                model.speed -= 0.1;
            }
            Key::Left => {
                model.scale_x -= 1.0;
                model.scale_y -= 1.0;
            }
            Key::Right => {
                model.scale_x += 1.0;
                model.scale_y += 1.0;
            }

            _ => {}
        },
        _ => {}
    }

    // println!(
    //     "Speed: {}, ScaleX: {}, ScaleY: {}",
    //     model.speed, model.scale_x, model.scale_y
    // );
}

fn update(_app: &App, model: &mut Model, update: Update) {
    let dt = update.since_last.as_secs_f64() * model.speed;

    model.time += dt;

    let len = model.agents.len() as f64;

    for agent in model.agents.iter_mut() {
        agent.update(model.noise.get([
            (model.scale_x * agent.u) + (agent.i / len * 10.0),
            (model.scale_y * agent.v),
            model.time,
        ]));
    }
}

fn view(app: &App, model: &Model, frame: Frame) {
    if app.elapsed_frames() < 5 {
        frame.clear(hsla(0.0, 0.0, 0.0, 0.001));
    }

    let draw = app.draw();
    let (w, h) = app.main_window().inner_size_pixels();

    // draw.rect()
    //     .x_y(0.0, 0.0)
    //     .w_h(w as f32, h as f32)
    //     .hsla(0.0, 0.0, 0.0, 0.025);

    for point in model.agents.iter() {
        let x = point.u * (w as f64 * 0.1);
        let y = point.v * (h as f64 * 0.5);

        draw.ellipse()
            .hsl(point.n as f32 * 0.1, 1.0, 0.5)
            .x_y(x as f32, y as f32)
            .radius(point.size as f32 * 5.0);
    }
    draw.to_frame(app, &frame).unwrap();
}
