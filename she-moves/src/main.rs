use nannou::noise::*;
use nannou::prelude::*;

mod agent;
use crate::agent::Agent;

const AGENT_COUNT: usize = 50;
const LAYER_COUNT: usize = 50;

fn main() {
    nannou::app(model).update(update).run()
}

struct Model {
    layers: Vec<Vec<Agent>>,
    noise: SuperSimplex,
    time: f32,
    scale_x: f32,
    scale_y: f32,
    speed: f32,
}

fn model(app: &App) -> Model {
    app.new_window().event(event).view(view).build().unwrap();

    let mut layers = vec![];
    let mut noise = SuperSimplex::new();
    noise = noise.set_seed(42);

    for i in 1..AGENT_COUNT {
        let mut layer = vec![];

        for j in 1..LAYER_COUNT {
            let u = i as f32 / AGENT_COUNT as f32;
            let v = (1.0 - (j as f32 / LAYER_COUNT as f32)).abs();

            layer.push(Agent::new(u, v, 5.0, i as u32));
        }

        layers.push(layer);
    }

    Model {
        layers,
        time: 0.0,
        noise,
        scale_x: 12.0,
        scale_y: 12.0,
        speed: 1.0,
    }
}

fn n(x: f32) -> f32 {
    return (x - 0.5) * 1.8;
}

fn normal_noise(x: f32) -> f32 {
    return (x + 1.0) / 2.0;
}

fn event(_app: &App, model: &mut Model, window_event: WindowEvent) {
    // println!("{:?}", window_event);

    match window_event {
        MouseMoved(pos) => {
            model.scale_x = pos.x * 0.1;
            model.scale_y = pos.y * 0.1;
        }
        KeyPressed(key) => match key {
            Key::Up => {
                model.speed += 0.1;
            }
            Key::Down => {
                model.speed -= 0.1;
            }
            _ => {}
        },
        _ => {}
    }

    println!(
        "Speed: {}, ScaleX: {}, ScaleY: {}",
        model.speed, model.scale_x, model.scale_y
    );
}

fn update(app: &App, model: &mut Model, _update: Update) {
    let dt = model.speed * 0.005; // a;pp.fps() / 60.0 * 0.005;

    model.time += dt;
    // println!("fps: {}, dt: {}, time: {}", app.fps(), dt, model.time);
}

fn view(app: &App, model: &Model, frame: Frame) {
    frame.clear(BLACK);
    let draw = app.draw();
    let (w, h) = app.main_window().inner_size_pixels();
    let w = w as f32;
    let h = h as f32;
    let noise_scale = 5.0;

    let size_scale = 10.0;
    let time_scale = 15.0;
    let t = model.time * time_scale;

    for layer in model.layers.iter() {
        for point in layer.iter() {
            let uu = point.u * model.scale_x;
            let vv = point.v * model.scale_y;
            let noise = model.noise.get([uu as f64, vv as f64, model.time as f64]) as f32;
            let nn = normal_noise(noise);

            let x = n(point.u) * w * 0.5;
            let y = (n(point.v) + noise * 0.06) * h * 0.5;

            draw.rect()
                .hsl(nn * 0.4 - 0.2, 1.0, 0.5)
                .x_y(x, y)
                .rotate(nn * nn * PI * 5.0)
                .w(nn * size_scale)
                .h(nn * size_scale);
        }
    }
    draw.to_frame(app, &frame).unwrap();
}
