use nannou::noise::*;
use nannou::prelude::*;

mod agent;
use crate::agent::Agent;

const AGENT_COUNT: usize = 20;
const LAYER_COUNT: usize = 20;

fn main() {
    nannou::app(model).update(update).simple_window(view).run()
}

struct Model {
    layers: Vec<Vec<Agent>>,
    noise: SuperSimplex,
    time: f32,
}

fn model(_app: &App) -> Model {
    let mut agents = vec![];
    let mut noise = SuperSimplex::new();
    noise = noise.set_seed(42);
    for i in 1..AGENT_COUNT {
        let u = i as f32 / AGENT_COUNT as f32;
        agents.push(Agent::new(u, (u * 10.0).sin().sin(), 5.0, i as u32, 0.0));
    }

    Model {
        layers: vec![agents],
        time: 2.0,
        noise,
    }
}

fn n(x: f32) -> f32 {
    return (x - 0.5) * 1.9;
}

fn dn(x: f32) -> f32 {
    return (x + 1.0) * 0.5;
}

fn update(app: &App, model: &mut Model, _update: Update) {
    let update_rate = app.fps() / 120.0 * 0.002;
    // let f = app.elapsed_frames();
    model.time += update_rate;

    if &model.layers.len() >= &LAYER_COUNT {
        model.layers.remove(0);
    }

    let mut layer = vec![];

    for i in 1..AGENT_COUNT {
        let u = i as f32 / AGENT_COUNT as f32;
        let noise = model.noise.get([20.0 * u as f64, 3.0 * model.time as f64]) as f32;

        layer.push(Agent::new(u, dn(noise), 5.0, i as u32, noise));
    }

    model.layers.push(layer);

    for layer in model.layers.iter_mut() {
        for point in layer.iter_mut() {
            point.update_offset(1.0 / LAYER_COUNT as f32);
        }
    }
}

fn view(app: &App, model: &Model, frame: Frame) {
    frame.clear(BLACK);
    let draw = app.draw();
    let (w, h) = app.main_window().inner_size_pixels();
    let w = w as f32;
    let h = h as f32;

    for layer in model.layers.iter() {
        for point in layer.iter() {
            // for point in model.agents.iter() {
            draw.ellipse()
                .hsla(
                    point.u + dn(point.n) * 0.2,
                    dn(point.n),
                    0.5,
                    0.2, // - (h * 0.1), // - point.v_offset,
                )
                .x_y(
                    (0.05 * point.n + n(point.u)) * (w - 20.0) / 2.0,
                    n(point.v_offset) * h * 0.05,
                )
                .w(17.0) //(point.u + model.time * PI * 2.0).sin() as f32 * point.size * 2.0)
                .h(7.0); // (point.u + model.time * PI * 3.0).cos() as f32 * agent.size * 2.0);
        }
    }
    draw.to_frame(app, &frame).unwrap();
}
