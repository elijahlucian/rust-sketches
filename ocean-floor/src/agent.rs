pub use std::f32::consts::PI;

pub struct Agent {
  pub u: f32,
  pub v: f32,
  pub n: f32,
  pub v_offset: f32,
  pub size: f32,
  pub index: u32,
}

impl Agent {
  pub fn new(u: f32, v: f32, size: f32, index: u32, n: f32) -> Agent {
    Agent {
      u,
      v,
      size,
      v_offset: 0.0,
      index,
      n,
    }
  }

  pub fn update_v(&mut self, time: f32) {
    self.v = ((self.u + time) * PI * 2.0).sin();
  }

  pub fn update_offset(&mut self, time: f32) {
    self.v_offset += 0.1;
  }
}
